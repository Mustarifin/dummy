<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'BMS Admin') }}</title>

        <link href="{{ asset('assets/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/style.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/colors/megna.css') }}" id="theme" rel="stylesheet">
        <style>
            .navbar-header, .fix-sidebar, .top-left-part{
                background: #41a99d !important
            }

            #side-menu > li > a.active i {
                color: #2d7b76;
            }

            #side-menu > li > a.active {
                border-left: 3px solid #41a99d;
                color: #2d7b76;
                font-weight: 500;
            }

            .bg-title .breadcrumb .active {
                color: #41a99d;
            }
        </style>
        @yield('styles')
        <link rel="shortcut icon" href="{{ asset('assets/plugins/images/favicon.png') }}">
    </head>
    <body class="fix-sidebar">
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <x-header/>
        <x-sidebar/>
        <div id="page-wrapper">
            <div class="container-fluid">
                {{ $slot }}
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> {{ date('Y') }} &copy bms </footer>
        </div>
        </div>

        <script src="{{ asset('assets/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/bootstrap/dist/js/tether.min.js') }}"></script>
        <script src="{{ asset('assets/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('assets/js/custom.min.js') }}"></script>
        @yield('scripts')
    </body>
</html>

<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
                    <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                    </span> 
                </div>
                <!-- /input-group -->
            </li>
            <li class="user-pro">
                <a href="#" class="waves-effect"><img src="{{ asset('assets/plugins/images/users/d1.jpg') }}" alt="user-img" class="img-circle"> <span class="hide-menu">Admin<span class="fa arrow"></span></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <form method="GET" action="{{ route('login') }}">
                                @csrf
                                <a href="" onclick="event.preventDefault();
                                                    this.closest('form').submit();"><i class="fa fa-power-off"></i> Logout</a>
                        </form>
                    </li>
                </ul>
            </li>
                <li class="nav-small-cap m-t-10">--- Main Menu</li>
                <li> <a href="{{ route('home') }}" class="waves-effect"><i class="fa fa-home p-r-10"></i> <span class="hide-menu">Home</span></a> </li>
    </div>
</div>
<!-- Left navbar-header end -->
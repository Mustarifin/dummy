<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Traits\Filter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Enums\GroupStatus;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, Filterable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'image',
        'longitude',
        'latitude'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at',
        'deleted_at',
        'image',
        'email_verified_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function userGroups()
    {
        return $this->hasMany(UserGroup::class);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    public function scopeStandBy($query, $group = null)
    {
        if ($group != null) {

            return $query->whereHas('userGroups', function($query) use ($group){
                      $query->where('group_id', $group->id)
                         ->orWhereHas('group', function($query){
                             $query->where('status', GroupStatus::DEACTIVE);
                       });
                    })->orWhereDoesntHave('userGroups');
                    
        }

        return $query->whereHas('userGroups', function($query) use ($group){
                     $query->whereHas('group', function($query){
                        $query->where('status', GroupStatus::DEACTIVE);
                     });
                })->orWhereDoesntHave('userGroups'); 
    }

    public function stillAvailable()
    {
        $userGroups = $this->userGroups()->whereHas('group', function($query){
            $query->where('status', GroupStatus::ACTIVE);
        })->get();
        
        return count($userGroups) <= 0  ? true : false;
    }
}

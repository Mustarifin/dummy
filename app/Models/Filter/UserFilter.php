<?php

namespace App\Models\Filter;

use App\Traits\Filter\Filter;
use Illuminate\Database\Eloquent\Builder;

class UserFilter extends Filter
{
    public function keyword($keyword): Builder
    {
        return $this->builder->where('name', 'like' ,'%'.$keyword.'%')
               ->orWhere('email','like' ,'%'.$keyword.'%');
    }
}

<?php

namespace App\Models\Filter;

use App\Traits\Filter\Filter;
use Illuminate\Database\Eloquent\Builder;

class GroupFilter extends Filter
{
    public function keyword($keyword): Builder
    {
        return $this->builder->where('name', 'like' ,'%'.$keyword.'%');
    }
}

<?php

namespace App\Http\Requests\Group;

use Illuminate\Foundation\Http\FormRequest;
use App\Enums\GroupStatus;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'   => 'required|max:50',
            'users'  => 'required',
            'status' => 'nullable' 
        ];
    }

    public function getData()
    {
        return [
            'name'   => $this->name,
            'status' => $this->status  == null ? GroupStatus::DEACTIVE : $this->status
        ];
    }
}
